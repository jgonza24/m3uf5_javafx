package problemes.quotes.impl;

import javafx.application.Application;
import javafx.stage.Stage;
import problemes.quotes.QuoteContract.QuoteModel;
import problemes.quotes.QuoteContract.QuotePresenter;
import problemes.quotes.QuoteContract.QuoteView;

public class QuoteApp extends Application {
	
	private QuoteView v;
	private QuotePresenter p;
	private QuoteModel m;
	
	@Override
	public void start(Stage primaryStage) {
		
		p = new QuotePresenterImpl();
		m = new QuoteModelImpl();		
		v = new QuoteViewImpl(primaryStage);
		
		p.setView(v);
		p.setModel(m);
		v.setPresenter(p);
	}
	
	@Override
	public void init() {		
	}
		

	
	public static void main(String[] args) {
		launch(args);
	}
}
