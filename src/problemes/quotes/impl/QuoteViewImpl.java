package problemes.quotes.impl;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import problemes.quotes.QuoteContract.QuotePresenter;
import problemes.quotes.QuoteContract.QuoteView;

public class QuoteViewImpl implements QuoteView {

	private QuotePresenter presenter;	
	private Label label;

	public QuoteViewImpl(Stage primaryStage) {
		
		initUI(primaryStage);
	}

	@Override
	public void setPresenter(QuotePresenter p) {
		this.presenter = p;
	}

	private void initUI(Stage stage) {
		
		VBox vb = new VBox(10); // gap between components is 20
		
		vb.setAlignment(Pos.CENTER); // center the components within VBox		
		vb.setPrefWidth(400);
		vb.setLayoutY(20); // start Y
		
		label = new Label();
		label.setWrapText(true);
		label.setPadding(new Insets(10));
		label.setPrefHeight(100);
		label.setText("");
		
		HBox hb = new HBox(10);
		hb.setAlignment(Pos.BASELINE_CENTER);
		
		Button nextBtn = new Button();
		nextBtn.setText("Next");
		nextBtn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				
				presenter.nextRequested();
			}			
		});
		
		
		Button deleteBtn = new Button();
		deleteBtn.setText("Delete");
		
		TextField newText = new TextField();
		
		Button addBtn = new Button();
		addBtn.setText("Add");
		
		hb.getChildren().addAll(nextBtn, deleteBtn, newText, addBtn);
		
		vb.getChildren().addAll(label, hb);
		
		Scene scene = new Scene(vb, 400, 200);
		stage.setScene(scene);
		stage.setTitle("Better Quotes");
		stage.show();			
	}

	@Override
	public void setQuoteText(String quote) {
		
		label.setText(quote);
	}

}
