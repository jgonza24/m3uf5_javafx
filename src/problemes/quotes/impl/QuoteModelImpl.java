package problemes.quotes.impl;

import problemes.quotes.QuoteContract.QuoteListener;
import problemes.quotes.QuoteContract.QuoteModel;

public class QuoteModelImpl implements QuoteModel {

    private int index;

    private final String[] QUOTES = new String[]{
        "No hay caminos para la paz; la paz es el camino. -Mahatma Gandhi",
        "Aprende a vivir y sabrás morir bien. -Confucio",
        "Cada día sabemos más y entendemos menos. -Albert Einstein",
        "El sabio no dice nunca todo lo que piensa, pero siempre piensa todo lo que dice. -Aristóteles",
        "La peor experiencia es la mejor maestra. -Kovo",
        "El pesimista se queja del viento; el optimista espera que cambie; el realista ajusta las velas.-William George War",
        " Ojo por ojo y el mundo acabará ciego. -Mahatma Gandhi",
        "Nunca rompas el silencio si no es para . -Beethoven",
        "La duda es la madre de la invención. -Galileo Galilei",
        "La inspiración existe, pero tiene que encontrarte trabajando. -Picasso",
        "Si ayer te caíste, levántate hoy. H.G. Wells",
        "El secreto de ir avanzando es empezar. -MarkTwain"
    };

    private QuoteListener listener;

    @Override
    public void requestNextQuote() {

        sendQuoteToListener();
    }

    @Override
    public void deleteCurrentQuote() {

        throw new RuntimeException("no implementat!");
    }

    @Override
    public void addNewQuote(String quote) {

        throw new RuntimeException("no implementat!");
    }

    @Override
    public void registerListener(QuoteListener l) {

        this.listener = l;
    }

    // PRIVATE
    /**
     * Send quote to Presenter
     */
    private void sendQuoteToListener() {

        if (++index >= QUOTES.length) {
            index = 0;
        }

        String text = QUOTES.length == 0 ? "" : QUOTES[index];
        this.listener.onNewQuote(text);
    }

}
