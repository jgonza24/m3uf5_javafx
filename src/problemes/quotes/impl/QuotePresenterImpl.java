package problemes.quotes.impl;

import javafx.application.Platform;
import problemes.quotes.QuoteContract.QuoteModel;
import problemes.quotes.QuoteContract.QuotePresenter;
import problemes.quotes.QuoteContract.QuoteView;

public class QuotePresenterImpl implements QuotePresenter {

	private QuoteView view;
	private QuoteModel model;

	@Override
	public void setView(QuoteView v) {
		this.view = v;
	}

	@Override
	public void setModel(QuoteModel m) {
		this.model = m;		
		initModel();
	}

	private void initModel() {
		
		this.model.registerListener(this);
	}

	@Override
	public void onNewQuote(String quote) {
		
		// from the model: send it to the view
		
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				view.setQuoteText(quote);
			}			
		});
	}

	@Override
	public void nextRequested() {
		
		// from the view:
		this.model.requestNextQuote();
	}

	@Override
	public void deleteRequested() {

		throw new RuntimeException("no implementat!");
	}

	@Override
	public void addRequested(String quote) {

		throw new RuntimeException("no implementat!");
	}

	
}
