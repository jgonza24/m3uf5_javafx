package problemes.quotes;

public interface QuoteContract {

    /**
     * the view contract
     */
    interface QuoteView {

        void setPresenter(QuotePresenter p);

        /**
         * the presented requested to change the quote text
         *
         * @param quote
         */
        void setQuoteText(String quote);

    }

    /**
     * the presenter contract
     */
    interface QuotePresenter extends QuoteListener {

        void setView(QuoteView v);

        void setModel(QuoteModel m);

        /**
         * the view next button was pushed
         */
        void nextRequested();

        /**
         * the view delete button was pushed
         */
        void deleteRequested();

        /**
         * the view add button was pushed
         *
         * @param quote
         */
        void addRequested(String quote);


    }

    /**
     * the model contract
     */
    interface QuoteModel {

        void registerListener(QuoteListener l);


        /**
         * the presenter asynchronously requests a new quote
         */
        void requestNextQuote();

        /**
         * the presenter requests to delete the current quote
         */
        void deleteCurrentQuote();

        /**
         * the presenter requests to add a new quote
         *
         * @param quote
         */
        void addNewQuote(String quote);

    }

    /**
     * the quote listener contract (the presenter is one)
     *
     */
    interface QuoteListener {

        /**
         * the listener receives a new quote
         *
         * @param quote
         */
        void onNewQuote(String quote);
    }
}
